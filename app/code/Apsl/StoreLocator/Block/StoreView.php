<?php
namespace Apsl\StoreLocator\Block;

use Magento\Framework\View\Element\Template;
use Apsl\StoreLocator\Model\ResourceModel\Store\CollectionFactory as StoresFactory;
/**
 * Class StoreList
 * @package Apsl\StoreLocator\Block
 */
class StoreList extends Template
{
    private $storeView;
    public function __construct(
        Template\Context $context,
        StoresFactory $storeView,
        array $data = []
    )
{
    parent::__construct($context,$data);
    $this->storeView = $storeView;
}


     public function getCurrentStore()
     {
         //to do info o sklepie
         return 'Something';
     }
}