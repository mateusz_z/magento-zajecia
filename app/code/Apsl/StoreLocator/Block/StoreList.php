<?php
/**
 * @author Arkadiusz Tokarczyk
 * @package Apsl_StoreLocator
 */

namespace Apsl\StoreLocator\Block;

use Magento\Framework\View\Element\Template;
use Apsl\StoreLocator\Model\ResourceModel\Store\CollectionFactory as StoresFactory;
/**
 * Class StoreList
 * @package Apsl\StoreLocator\Block
 */
class StoreList extends Template
{
    private $storeList;
    public function __construct(
        Template\Context $context,
        StoresFactory $storeList,
        array $data = []
    )
{
    parent::__construct($context,$data);
    $this->storeList = $storeList;
}
public function getAvailableStores(){
    $stores = $this->storeList->create();
    $stores->addFieldToFilter('is_active','1');
    return $stores;
}
    // public function getSomething()
    // {
    //     return 'Something';
    // }
}