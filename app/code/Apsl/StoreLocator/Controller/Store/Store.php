<?php

namespace Apsl\StoreLocator\Controller\Store;
use Apsl\StoreLocator\Model\StoreFactory;
use Apsl\StoreLocator\Model\ResourceModel\Store as StoreResource;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Registry;

class Store extends Action
{
    /** @var PageFactory */
    private $resultPageFactory;

    private $registry;

    private $storeModel;

    private $storeResource;

    /**
     * Stores constructor.
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(Context $context, PageFactory $resultPageFactory,Registry $registry,StoreFactory $storeModel, StoreResource $storeResource)
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->registry = $registry;
        $this->storeModel = $storeModel;
        $this->storeResource = $storeResource;
    }

    public function execute()
    {
        $request = $this->getRequest();
        $storeId = $request->getParam('id',null);

        if (is_null($storeId)){
            die(var_dump('missing id param'));
        }

        $storeModel = $this->storeModel->create();
        $this->storeResource->load($storeModel,$storeId);

        $this->registry->register('apslCurrentStore',$storeModel);

        $resultPage = $this->resultPageFactory->create();
        return $resultPage;
    }
}