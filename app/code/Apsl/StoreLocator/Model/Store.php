<?php 
namespace Apsl\StoreLocator\Model;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

class Store extends AbstractModel implements IdentityInterface
{
    const CACHE_TAG = 'apsl_storelocator_store';
    protected function _construct()
    {
        $this->_init('Apsl\StoreLocator\Model\ResourceModel\Store');
    }
    public function getIdentities()
    {
        return[self::CACHE_TAG . '_' . $this->getId()];
    }
}