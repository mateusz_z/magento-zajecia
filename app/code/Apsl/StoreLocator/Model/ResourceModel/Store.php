<?php
namespace Apsl\StoreLocator\Model\ResourceModel;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class Store
 * @package Apsl\StoreLocator\Model\ResourceModel
 */


class Store extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('apsl_storelocator_store','store_id');
    }
}