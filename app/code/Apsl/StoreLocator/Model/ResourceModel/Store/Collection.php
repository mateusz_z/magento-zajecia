<?php
namespace Apsl\StoreLocator\Model\ResourceModel\Store;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Store
 * @package Apsl\StoreLocator\Model|ResourceModel
 */


class Collection extends AbstractCollection
{
    protected $_idFieldName = 'store_id';
    protected function _construct()
    {
        $this->_init('Apsl\StoreLocator\Model\Store','Apsl\StoreLocator\Model\ResourceModel\Store');
    }
}